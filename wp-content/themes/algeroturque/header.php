<?php
/**
 * Header file for the Algero Turque Plateforme WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Algeroturque
 * @since Algero Turque 1.0
 */

?><!DOCTYPE html>
<!--
    ____                  __         ______          __
   / __ \____ _____  ____/ ____     /_  _____  _____/ /_
  / /_/ / __ `/ __ \/ __  / __ \     / / / _ \/ ___/ __ \
 / ____/ /_/ / / / / /_/ / /_/ /    / / /  __/ /__/ / / _
/_/    \__,_/_/ /_/\__,_/\____/    /_/  \___/\___/_/ /_(_)
                                 https://pando.technology
-->
<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <header id="site-header">
            <div class="logo">
                <a href="<?php echo(get_site_url()); ?>">
                    <img src="<?php echo(get_template_directory_uri() . '/images/logo.png') ?>" alt="logo">
                </a>
            </div>
            <div class="menu">
                <?php
                    wp_nav_menu(
                    array(
                        'theme_location' => 'primary'
                    )
                    )
                ?>
            </div>
        </header>
