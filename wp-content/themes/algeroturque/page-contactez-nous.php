<?php get_header();?>
<div id="contenu" class="contact">
    <div class="container">
        <div class="page-title">
            <h1><?php the_title(); ?></h1>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo do_shortcode("[wpforms id='91']"); ?> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if(have_posts()): while(have_posts()): the_post(); ?>
                    <?php 
                        $address = get_post_meta($post->ID, 'address', true);
                        $phone = get_post_meta($post->ID, 'phone', true);
                        $mail = get_post_meta($post->ID, 'mail', true);
                        $facebook = get_post_meta($post->ID, 'facebook', true);
                        $instagram = get_post_meta($post->ID, 'instagram', true);
                        $youtube = get_post_meta($post->ID, 'youtube', true);
                        $linkedin = get_post_meta($post->ID, 'linkedin', true);
                    ?>
                    <div id="contact-info-block">
                        <p><i class="fas fa-map-marked"></i><?php echo(" " . $address); ?></p>
                        <p><i class="fas fa-paper-plane"></i><?php echo(" " . $mail); ?></p>
                        <p><i class="fas fa-phone"></i><?php echo(" " . $phone); ?></p>
                    </div>
                <?php endwhile; else: endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo do_shortcode("[wpgmza id='1']"); ?> 
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>
    