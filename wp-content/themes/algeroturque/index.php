<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage algeroturque
 * @since algeroturque 1.0
 */

get_header();
?>
<main id="main-contenu" role="main">
<div class="main-slider">
    <?php echo do_shortcode("[slide-anything id='60']"); ?> 
</div>
<section id="services" class="container">
        <div class="row">
            <h1 class="section-title">services</h1>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="service">
                    <?php echo file_get_contents(get_template_directory_uri() . '/images/svg/product.svg'); ?>
                    <h3>Produits</h3>
                </div>    
            </div>
            <div class="col-lg-4">
                <div class="service">
                    <?php echo file_get_contents(get_template_directory_uri() . '/images/svg/presentation.svg'); ?>
                    <h3>Exposition</h3>
                </div>    
            </div>
            <div class="col-lg-4">
                <div class="service">
                    <?php echo file_get_contents(get_template_directory_uri() . '/images/svg/care.svg'); ?>
                    <h3>Santé</h3> 
                </div>   
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="service">
                    <?php echo file_get_contents(get_template_directory_uri() . '/images/svg/wedding.svg'); ?>
                    <h3>Voyage de noces</h3>
                </div>    
            </div>
            <div class="col-lg-4">
                <div class="service">
                    <?php echo file_get_contents(get_template_directory_uri() . '/images/svg/home.svg'); ?>
                    <h3>Immobilier</h3> 
                </div>   
            </div>
            <div class="col-lg-4">
                <div class="service">
                    <?php echo file_get_contents(get_template_directory_uri() . '/images/svg/plane.svg'); ?>
                    <h3>Voyage organisé</h3>
                </div>    
            </div>
            
        </div>
        <div class="row"><a href="" id="autres">Et bien d'autres services</a></div>
</section>
<section id="about-us" class="container-fluid">
    <div class="row">
        <div class="col-lg-6" style="padding:0px;">
            <div id="about-us-text-block">
                <h1><?php echo(get_the_title(get_page_by_path('a-propos')));?></h1>
                <p><?php echo(get_the_excerpt(get_page_by_path('a-propos'))); ?></p>
            </div>
        </div>
        <div class="col-lg-6" style="padding:0px;">
        <img src="<?php echo(get_the_post_thumbnail_url(get_page_by_path('a-propos'), 'post-thumbnail')) ?>" alt="the team" style="width:100%">
        </div>
    </div>
</section>
<section id="partners" class="container">
    <div class="row">
        <h1 class="section-title">Nos partenaires</h1>   
    </div>
    <div class="row">
        <div class="partner-imgs">
                <?php 
                    $partners = get_attached_media(get_page_by_path('partners'));
                    foreach ($partners as $partner) : ?>
                        <img src="<?php echo($partner->guid); ?>" alt="partner image">
                    <?php endforeach; ?>
        </div>
    </div>
</section>
</main>
<?php
get_footer();
