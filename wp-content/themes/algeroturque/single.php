<?php get_header();?>
<div id="contenu">
    <div class="container">
        <div class="page-title">
            <h1><?php the_title(); ?></h1>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if(have_posts()): while(have_posts()): the_post(); ?>

                    <?php the_content(); ?>

                <?php endwhile; else: endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>
