<?php get_header();?>
<div id="contenu">
    <div class="container">
        <div class="page-title">
            <h1><?php the_title(); ?></h1>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if(have_posts()): while(have_posts()): the_post(); ?>

                    <?php the_content(); ?>

                <?php endwhile; else: endif; ?>
            </div>
        </div>
        <br><br><br>
        <div class="row">
            <div class="col-lg-12">
                <center><h3>Programmes</h3></center>
                <div class="programmes">        
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/cancer.jpg'); ?>) center center;" ><span>PROGRAMME COMPLET DE LUTTE CONTRE LE CANCER</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/cardio.jpg'); ?>) center center;" ><span>CENTRE DE TRANSPLANTATION D’ORGANES</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/chirurgie.jpg'); ?>) center center;" ><span>NEUROCHIRURGIE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/dermatologie.jpg'); ?>) center center;" ><span>NUTRITION ET RÉGIME</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/organes.jpg'); ?>) center center;" ><span>GYNÉCOLOGIE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/genico.jpg'); ?>) center center;" ><span>CARDIOLOGIE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/Neurochirurgie.jpg'); ?>) center center;" ><span>ORTHOPÉDIE ET TRAUMATOLOGIE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/neurologie.jpg'); ?>) center center;" ><span>CENTRE DE SOINS AMBULATOIRES ET DE CHIRURGIE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/nutrition.jpg'); ?>) center center;" ><span>OXYGÉNOTHÉRAPIE SOUS-MARINE ET HYPERBARE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/orthopedie.jpg'); ?>) center center;" ><span>NEUROLOGIE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/oxyno.jpeg'); ?>) center center;" ><span>CHIRURGIE CARDIOVASCULAIRE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/plastic.jpg'); ?>) center center;" ><span>DERMATOLOGIE</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/reeducation.jpg'); ?>) center center;" ><span>TRAITEMENT PHYSIQUE ET RÉÉDUCATION</span></a></div>
                    <div class="parent"><a class="child" href="<?php echo(get_page_link(get_page_by_path('contactez-nous'))); ?>" style="background: url(<?php echo(get_template_directory_uri() . '/images/programmes/vasculaire.jpg'); ?>) center center;" ><span>ESTHÉTIQUE ET CHIRURGIE PLASTIQUE</span></a></div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<?php get_footer();?>
