<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Algeroturque
 * @since Algero Turque 1.0
 */
?>

<footer id="site-footer" role="contentinfo" class="header-footer-group">
        <p class="footer-copyright">&copy;
            <?php 	echo date_i18n(_x( 'Y', 'copyright date format', 'Algero Turque' )); ?>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <?php bloginfo( 'name' ); ?>
            </a>
        </p>
        <a class="to-the-top" href="#site-header">
            <!-- <span class="to-the-top-long">
                <?php //printf( __( 'To the top %s', 'algeroturque' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' ); ?>
            </span> -->
            <span class="to-the-top-short">
                <?php printf( __( 'Up %s', 'algeroturque' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' ); ?>
            </span>
        </a>
</footer>
<?php wp_footer(); ?>
	</body>
</html>
