<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'algeroturc' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:8888' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>s96XfS&70d9Jl1,)5~!bP);F;;/K?YBCr7Q-1Kgn@[Kf2Q/5G{~]2~Tfg%%X@kE' );
define( 'SECURE_AUTH_KEY',  '#K38>hgMSBgECxp6=wJ(*^:Xz|t-WLln42HR%1jF@@,EicC@={nup=l2e`Df^9~G' );
define( 'LOGGED_IN_KEY',    's4#*[|W)Uof1 2L%Tvt{w3I0gg~|>%fo#Et?p*KN +X&W:y>[8[[x[)6iW`Jh)BZ' );
define( 'NONCE_KEY',        'T5^[O#cM}SU86_xW?^HtzbDAfP;GPVpcS]ckc7Ei?a7x9tl)@Kd*!EWCIj<gW/%+' );
define( 'AUTH_SALT',        'uI. pVI`i,S}GrWJ}A06Z/v-s=6W{%Fyy*jEKYto71DU5#u?JJP;W9qr*8YWpN k' );
define( 'SECURE_AUTH_SALT', 'i<(`m9KTMuCRg>?-!l6+|wV8[<H)6KZ=RWl>Ho%m4/|w?;rj[^^t=O%!g,#ON5:)' );
define( 'LOGGED_IN_SALT',   '%;@Xdk[]$XWg &|r0?l)9NfYlQ8EB 4HLe~PSAW3&<78.`T0V%hv1Zq[`W5B4e|Z' );
define( 'NONCE_SALT',       'bQZF>f:*p&TDd?xx]glhdHZ-kP yE<39.EKU9(zaLtAv{Q7~0{7H0OrtDqht:p5>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true  );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
